# Spring Boot Admin Dashboard

Run this project by this command : `mvn clean spring-boot:run`

Username & Password : admin / password

### Screenshot

Login Page

![Login Page](img/login.png "Login Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")

Wallboard Page

![Wallboard Page](img/wallboard.png "Wallboard Page")

About Page

![About Page](img/about.png "About Page")

